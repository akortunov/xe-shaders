XE Shaders
==========

This is a port of built-in postprocessing shaders from MGE XE to OpenMW. Requires OpenMW 0.48 or newer.

Installation
------------
1. Copy L10n and Shaders folders to the desired folder and register that desired folder in the openmw.cfg as a data folder:

data="/path/to/directory/folder_name"

2. Enable postprocessing via settings in-game menu.
3. Configure a hotkey so access postprocess HUD via in-game settings menu.
4. Enable shaders which you want to use via postprocess HUD by moving them to the right column.

Note: if you use bloom shaders, it is recommended to do not have the "clamp lighting = false" in your settings.cfg.
Otherwise some textures (e.g. snow) will be too bright during clear weather (because this setting increases brightness in bright scenes as well as shaders).
