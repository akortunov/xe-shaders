uniform_int bloom_quality
{
    default = 3;
    min = 1;
    max = 3;
    step = 1;
    display_name = "#{XEShaders:BloomSoftQuality}";
    description = "#{XEShaders:BloomSoftQualityDescription}";
}

uniform_float bloom_mult_global
{
    default = 0.22;
    min = 0.0;
    max = 1.0;
    step = 0.01;
    display_name = "#{XEShaders:BloomSoftOverallMultiplier}";
    description = "#{XEShaders:BloomSoftOverallMultiplierDescription}";
}

uniform_float bloom_mult_uwater
{
    default = 1.2;
    min = 0.0;
    max = 10.0;
    step = 0.1;
    display_name = "#{XEShaders:BloomSoftUnderwaterMultiplier}";
    description = "#{XEShaders:BloomSoftUnderwaterMultiplierDescription}";
}

uniform_float bloom_mult_inside
{
    default = 1.1;
    min = 0.0;
    max = 10.0;
    step = 0.1;
    display_name = "#{XEShaders:BloomSoftInteriorMultiplier}";
    description = "#{XEShaders:BloomSoftInteriorMultiplierDescription}";
}

uniform_float bloom_mult_outside
{
    default = 0.8;
    min = 0.0;
    max = 10.0;
    step = 0.1;
    display_name = "#{XEShaders:BloomSoftExteriorMultiplier}";
    description = "#{XEShaders:BloomSoftExteriorMultiplierDescription}";
}

uniform_float bloom_cutoff
{
    default = 0.72;
    min = 0.5;
    max = 1.0;
    step = 0.02;
    display_name = "#{XEShaders:BloomSoftCutoff}";
    description = "#{XEShaders:BloomSoftCutoffDescription}";
}

uniform_float bloom_power
{
    default = 1.1;
    min = 0.5;
    max = 5.0;
    step = 0.1;
    display_name = "#{XEShaders:BloomSoftPower}";
    description = "#{XEShaders:BloomSoftPowerDescription}";
}

uniform_bool first_person_bloom
{
    default = false;
    display_name = "#{XEShaders:BloomSoftFirstPersonBloom}";
    description = "#{XEShaders:BloomSoftFirstPersonBloomDescription}";
}

shared
{
    const float sky = 1e6;
    vec2 rcpres = 1.0 / omw.resolution;
}

fragment blurHorI
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        float div = 4.2;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;

        if (bloom_quality >= 2)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;
            div = 4.7;

            if (bloom_quality >= 3)
            {
                color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 5.0), omw_TexCoord.y)) * 0.15;
                color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 5.0), omw_TexCoord.y)) * 0.15;

                color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 6.0), omw_TexCoord.y)) * 0.10;
                color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 6.0), omw_TexCoord.y)) * 0.10;
                div = 5.2;
            }
        }

        color /= div;

        omw_FragColor = color * color;
    }
}

fragment blurHor2
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        float div = 4.2;

        float spread = 1.0 - (color.r * 0.3) + (color.g * 0.6) + (color.b * 0.1);
        spread = smoothstep(0.0, 0.7, spread) * 4.0 + 1.0;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 1.0), omw_TexCoord.y)) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 1.0), omw_TexCoord.y)) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 2.0), omw_TexCoord.y)) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 2.0), omw_TexCoord.y)) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 3.0), omw_TexCoord.y)) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 3.0), omw_TexCoord.y)) * 0.4;

        if (bloom_quality >= 2)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 4.0), omw_TexCoord.y)) * 0.25;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 4.0), omw_TexCoord.y)) * 0.25;
            div = 4.7;
        }

        color /= div;

        omw_FragColor = color;
    }
}

fragment blurHor3
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        if (bloom_quality <= 1)
        {
            omw_FragColor = color;
            return;
        }

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;

        float div = 4.7;

        if (bloom_quality >= 3)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.y * 5.0), omw_TexCoord.y)) * 0.15;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.y * 5.0), omw_TexCoord.y)) * 0.15;

            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.y * 6.0), omw_TexCoord.y)) * 0.10;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.y * 6.0), omw_TexCoord.y)) * 0.10;
            div = 5.2;
        }

        color /= div;

        omw_FragColor = color;
    }
}

fragment blurVertI
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        float div = 4.2;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 1.0), omw_TexCoord.y)) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 2.0), omw_TexCoord.y)) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 3.0), omw_TexCoord.y)) * 0.4;

        if (bloom_quality >= 2)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * 4.0), omw_TexCoord.y)) * 0.25;
            div = 4.7;

            if (bloom_quality >= 3)
            {
                color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.y * 5.0), omw_TexCoord.y)) * 0.15;
                color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.y * 5.0), omw_TexCoord.y)) * 0.15;

                color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.y * 6.0), omw_TexCoord.y)) * 0.10;
                color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.y * 6.0), omw_TexCoord.y)) * 0.10;
                div = 5.2;
            }
        }

        color /= div;

        omw_FragColor = color;
    }
}

fragment blurVert2
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        float div = 4.2;

        float spread = 1.0 - (color.r * 0.3) + (color.g * 0.6) + (color.b * 0.1);
        spread = smoothstep(0.0, 0.7, spread) * 4.0 + 1.0;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 1.0), omw_TexCoord.y)) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 1.0), omw_TexCoord.y)) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 2.0), omw_TexCoord.y)) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 2.0), omw_TexCoord.y)) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 3.0), omw_TexCoord.y)) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 3.0), omw_TexCoord.y)) * 0.4;

        if (bloom_quality >= 2)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x - (rcpres.x * spread * 4.0), omw_TexCoord.y)) * 0.25;
            color += omw_GetLastPass(vec2(omw_TexCoord.x + (rcpres.x * spread * 4.0), omw_TexCoord.y)) * 0.25;
            div = 4.7;
        }

        color /= div;

        omw_FragColor = color;
    }
}

fragment blurVert3
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 color = omw_GetLastPass(omw_TexCoord);

        if (bloom_quality <= 1)
        {
            omw_FragColor = color;
            return;
        }

        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 1.0))) * 0.8;
        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 1.0))) * 0.8;

        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 2.0))) * 0.65;
        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 2.0))) * 0.65;

        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 3.0))) * 0.4;
        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 3.0))) * 0.4;

        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 4.0))) * 0.25;
        color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 4.0))) * 0.25;

        float div = 4.7;

        if (bloom_quality >= 3)
        {
            color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 5.0))) * 0.15;
            color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 5.0))) * 0.15;

            color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y - (rcpres.y * 6.0))) * 0.10;
            color += omw_GetLastPass(vec2(omw_TexCoord.x, omw_TexCoord.y + (rcpres.y * 6.0))) * 0.10;
            div = 5.2;
        }

        color /= div;

        omw_FragColor = color;
    }
}

fragment brightPass
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 c = omw_GetLastPass(omw_TexCoord);
        if (bloom_quality <= 1)
        {
            omw_FragColor = c;
            return;
        }

        vec4 cB = c;

        c = smoothstep(0.0, bloom_cutoff, c);
        c = pow(c, vec4(bloom_power));
        c += smoothstep(bloom_cutoff, 0.5 + bloom_cutoff, cB);
        c.a = 1.0;

        omw_FragColor = c;
    }
}

fragment recombA
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        if (bloom_quality <= 1)
        {
            omw_FragColor = omw_GetLastPass(omw_TexCoord);
            return;
        }

        float fogLevel = omw_EstimateFogCoverageFromUV(omw_TexCoord);
        vec4 cB = omw_GetLastShader(omw_TexCoord);
        cB.rgb *= 1.0 - fogLevel;

        if (!first_person_bloom)
        {
            float d = min(omw_GetLinearDepth(omw_TexCoord), sky);
            cB.rgb *= smoothstep(35.0, 50.0, d);
        }

        vec4 c = omw_GetLastPass(omw_TexCoord);

        c = smoothstep(0.0, bloom_cutoff, c);
        c += smoothstep(bloom_cutoff - 0.5, bloom_cutoff + 0.5, cB);

        omw_FragColor = clamp(c, 0.0, 1.0);
    }
}

fragment tB0
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        // grab scene and blurred highlights
        vec4 scene = omw_GetLastShader(omw_TexCoord);
        vec4 blur = omw_GetLastPass(omw_TexCoord);
        vec4 highlights = vec4(0.0);

        // apply all needed mults
        blur *= bloom_mult_global;

        if (omw.isInterior)
            blur *= bloom_mult_inside;
        else
            blur *= bloom_mult_outside;

        if (omw.isUnderwater)
            blur *= bloom_mult_uwater;

        scene += blur;

        omw_FragColor = scene;
    }
}

fragment doDepthFix
{
    omw_In vec2 omw_TexCoord;

    void main()
    {
        float fogLevel = omw_EstimateFogCoverageFromUV(omw_TexCoord);
        vec4 color = omw_GetLastShader(omw_TexCoord);
        color.rgb *= 1.0 - fogLevel;

        if (!first_person_bloom)
        {
            float d = min(omw_GetLinearDepth(omw_TexCoord), sky);
            color.rgb *= smoothstep(35.0, 50.0, d);
        }

        omw_FragColor = color;
    }
}

technique
{
    passes = doDepthFix, blurHorI, blurVertI, brightPass, blurHor2, blurVert2, recombA, blurHor3, blurVert3, tB0;
    description = "#{XEShaders:BloomSoftDescription}";
    author = "akortunov, MGE team";
    version = "1.0";
}
